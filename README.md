To run tests locally execute command 
```
mvn clean verify
```

I added a negative scenario when the user tries execute put delete and post call and get code "405 Not allowed"
(invalid_method_call.feature).
 1. Added tests which ensure that user get at least 1 product from  call "https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} (search_product.feature) otherwise test will be failed.
 2. Currently, there is failed test for orange because url https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange returns empty response (I assume that at least one product is shown).
 3. Added test which ensure that user get Not found status code from call "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apples" (non_existing_url.feature) because product apples doesn't exist. 
 4. Added custom ParameterType Method for handle methods like PUT/POST/DELETE/GET so now text like "he calls method put" will extract put and handle it with this code:
   ```
   @When("he calls method {method} to url {string}")
   heCallsEndpoint(Method methodName, String product)
```
 To view html report make some commits and then go  Report  and click browse under Job artifacts then click on public folder and click index.html . Report example https://test1302544.gitlab.io/-/testtask/-/jobs/5464481888/artifacts/public/index.html
