package starter.search;


import net.thucydides.core.annotations.Step;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class ProductSearchResult {
    @Step("count products")
    public Integer getProductCount() {
        List providers = lastResponse().body().jsonPath().get("provider");
        return providers.size();
    }
}
