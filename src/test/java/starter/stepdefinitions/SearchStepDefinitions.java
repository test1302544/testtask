package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.eclipse.jetty.http.HttpStatus;
import starter.search.ProductSearchResult;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class SearchStepDefinitions {

    @Steps
    ProductSearchResult productSearchResult;

    @Given("he search for product")
    public void user_call_for_product() {

    }

    @When("he calls url {string}")
    public void heCallsUrl(String product) {
        SerenityRest.given().get(product);
    }

    @Then("he sees the results displayed about {string}")
    public void heSeesTheResultsDisplayedForProduct(String product) {
        restAssuredThat(response -> response.statusCode(HttpStatus.OK_200));
        assert productSearchResult.getProductCount() > 0;
    }

}
