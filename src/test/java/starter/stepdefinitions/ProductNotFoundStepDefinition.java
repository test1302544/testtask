package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.eclipse.jetty.http.HttpStatus;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ProductNotFoundStepDefinition {
    @When("he calls url {string} for non existing product")
    public void heCallsNonExistingProduct(String productUrl) {
        SerenityRest.given().get(productUrl);
    }

    @Then("he sees page not found error")
    public void heSeesPageNotFoundError() {
        restAssuredThat(response -> response.statusCode(HttpStatus.NOT_FOUND_404));
    }

}
