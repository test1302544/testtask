package starter.stepdefinitions;

import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.apache.commons.lang3.EnumUtils;
import org.eclipse.jetty.http.HttpStatus;
import starter.http.Method;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class InvalidMethodStepDefinitions {
    @ParameterType("put|delete|post")
    public Method method(String method) {
        return EnumUtils.getEnumIgnoreCase(Method.class, method);
    }

    @Given("he testing product url with different methods")
    public void user_call_for_product() {

    }

    @When("he calls method {method} to url {string}")
    public void heCallsEndpoint(Method methodName, String product) {
        switch(methodName){
            case PUT:
                SerenityRest.given().put(product);
                break;
            case DELETE:
                SerenityRest.given().delete(product);
                break;
            case POST:
                SerenityRest.given().post(product);
                break;
        }


    }

    @Then("he sees the error")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(HttpStatus.METHOD_NOT_ALLOWED_405));

    }
}
