Feature: Testing Invalid Method call
  Scenario: Edit apple url
    Given he testing product url with different methods
    When he calls method put to url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the error
  Scenario: Delete apple url
    Given he testing product url with different methods
    When he calls method delete to url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the error
  Scenario: Post apple url
    Given he testing product url with different methods
    When he calls method post to url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the error