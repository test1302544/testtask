Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios
  @apple
  Scenario: Search for 'apple'
    Given he search for product
    When he calls url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the results displayed about "apple"
  @orange
  Scenario: Search for 'orange'
    Given he search for product
    When he calls url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
    Then he sees the results displayed about "orange"

  @pasta
  Scenario: Search for 'pasta'
    Given he search for product
    When he calls url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/pasta"
    Then he sees the results displayed about "pasta"

  @cola
  Scenario: Search for 'cola'
    Given he search for product
    When he calls url "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the results displayed about "cola"




